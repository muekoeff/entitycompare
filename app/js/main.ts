import LabelLoader from './labelLoader';
import Permalink from './permalink';
import Settings from './settings';
import TaskQueue from './taskQueue';
import Ui from './ui';
import Utils from './utils';

let userInput: string[] = [];
let properties: string[] = [];
const queue = new TaskQueue(5);
const labelLoader = new LabelLoader(queue);

jQuery(function() {
	let toggle_wdid = false;
	$('#button-parse').removeAttr('disabled').on('click', function(e) {
		e.preventDefault();
		$(this).attr('disabled', 'disabled');

		userInput = parseUserInput();

		// Reset table
		if(userInput.length > 0) {
			$('#table-output thead tr').html('<th></th>');
			$('#table-output tbody').html('');

			generateTable(userInput);
		} else {
			$('#table-output thead tr').html('<th scope="col">&nbsp;</th>');
			$('#table-output tbody tr').html('<td>No data</td>');
			$(this).removeAttr('disabled');
			return;
		}
	});
	$('a[href="#fullscreen"]').on('click', function(e) {
		e.preventDefault();
		$('#table-container').toggleClass('fullscreen');
	});
	$('a[href="#toggle_wdid"]').on('click', function(e) {
		e.preventDefault();
		$('*[data-wdid]').each(function(i, val) {
			if(typeof $(val).attr('data-label') != 'string') $(val).attr('data-label', $(val).text());
			if(toggle_wdid) {
				$(this).text('Display Wikidata IDs');
				$(val).text($(val).attr('data-label'));
			} else {
				$(this).text('Display labels');
				$(val).text($(val).attr('data-wdid'));
			}
		});
		toggle_wdid = !toggle_wdid;
	});

	Settings.initialize();

	// Permalink
	$('#button-permalink').on('click', function(e) {
		Permalink.uiShown(userInput, properties);
	});
	Permalink.load();
});

function generateTable(userInput: string[]) {
	generateColumns(userInput);
	getEntities(userInput, [], function(e) {
		if(Object.keys(e).length > 0) {
			// Update entities on UI
			updateEntities(e);
			$('#table-output tbody').html('');

			// Add aliases-rows on UI
			let cells = '';
			let maxlength = 0;
			userInput.forEach(function(wdId: string, _i: number) {
				if(typeof e[wdId].aliases != 'undefined') {
					let length = 0;
					cells += `<td data-entity="${wdId}"><div><ul class="reordableList">`;
					$.each(e[wdId].aliases, function(_i, lang) {
						$.each(lang, function(i, val) {
							cells += `<li class="${val.language == Settings.getLanguage() ? 'highlight' : ''}">${wdDisplayAlias(val)}</li>`;
							length++;
						});
					});
					cells += '</ul></div></td>';
					maxlength = length > maxlength ? length : maxlength;
				} else {
					cells += `<td class="snak-undefined" data-entity="${wdId}"><i>Undefined</i></td>`;
				}
			});
			$('#table-output tbody').append(generateRow('Aliases', undefined, cells, maxlength, undefined, (<HTMLInputElement>$('#setting-ui-collapsealiases')[0]).checked));

			// Add description-rows on UI
			cells = '';
			maxlength = 0;
			userInput.forEach(function(wdId, i) {
				if(typeof e[wdId].descriptions != 'undefined') {
					let length = 0;
					cells += `<td data-entity="${wdId}"><div><ul class="reordableList">`;
					$.each(e[wdId].descriptions, function(i, val) {
						cells += `<li class="${val.language == Settings.getLanguage() ? 'highlight' : ''}"><span class="bubble">${val.language}</span> ${val.value}</li>`;
						length++;
					});
					cells += '</ul></div></td>';
					maxlength = length > maxlength ? length : maxlength;
				} else {
					cells += `<td class="snak-undefined" data-entity="${wdId}"><i>Undefined</i></td>`;
				}
			});
			$('#table-output tbody').append(generateRow('Descriptions', undefined, cells, maxlength, undefined, true));

			// Add sitelink-rows on UI
			cells = '';
			maxlength = 0;
			userInput.forEach(function(wdId, i) {
				if(typeof e[wdId].sitelinks != 'undefined') {
					let length = 0;
					cells += `<td data-entity="${wdId}"><div><ul>`;
					$.each(e[wdId].sitelinks, function(i, val) {
						cells += `<li>${wdDisplaySitelink(val)}</li>`;
						length++;
					});
					cells += '</ul></div></td>';
					maxlength = length > maxlength ? length : maxlength;
				} else {
					cells += `<td class="snak-undefined" data-entity="${wdId}"><i>Undefined</i></td>`;
				}
			});
			$('#table-output tbody').append(generateRow('Sitelinks', undefined, cells, maxlength, undefined, (<HTMLInputElement>$('#setting-ui-collapsesitelinks')[0]).checked));

			// Add property-rows on UI
			properties = listProperties(e);
			const whitelist = Permalink.getFilteredProperties();
			const whitelistedProperties = (whitelist == null ? properties : properties.filter(function(el) {
				return whitelist.includes(el);
			}));
			whitelistedProperties.forEach(function(property, i) {
				cells = '';
				maxlength = 0;

				const row = generateRow(`<a href="https://www.wikidata.org/wiki/Property:${property}" target="_blank" title="${property}" data-wdid="${property}">${property}</a>`, property, null, maxlength);

				userInput.forEach(function(wdId, i) {
					if(typeof e[wdId].claims != 'undefined' && typeof e[wdId].claims[property] != 'undefined') {
						let length = 0;
						const listCell = $(`<td data-entity="${wdId}"><div><ul></ul></div></td>`);
						$.each(e[wdId].claims[property], function(i, val) {
							const listItem = $('<li></li>');
							listItem.append(wdDisplayValue(val));
							listCell.find('ul').append(listItem);
							length++;
						});
						row.append(listCell);
						maxlength = length > maxlength ? length : maxlength;
					} else {
						row.append(`<td class="snak-undefined" data-entity="${wdId}"><i>Undefined</i></td>`);
					}
				});
				$('#table-output tbody').append(row);
			});

			// Add collapse event handler
			$('#table-container a[href=\'#collapse\']').on( 'click' ,function(e) {
				e.preventDefault();
				$(this).parent().parent().toggleClass('collapsed');
				if($(this).text() == '[–]') {
					$(this).text('[+]');
				} else {
					$(this).text('[–]');
				}
			});
			updateProperties(properties);
			labelLoader.startWork();

			// Enable permalink-button
			$('#button-permalink').removeClass('disabled');
		} else {
			console.warn('No results');
		}
	});
	function generateColumns(userInput: string[]) {
		$.each(userInput, function(colIndex, wdId) {
			const cell = $(`<th scope="col" ${colIndex == 0 ? 'class="reference-item" ' : ''} data-entity="${wdId}"><a href="https://www.wikidata.org/wiki/${wdId}" target="_blank" title="${wdId}">${Ui.getIdLabel(wdId, null)}</a></th>`);
			$('#table-output thead tr').append(cell);
		});
	}
	function generateRow(title: string, property: string, cells: string, maxElementsInCell: number, classes = '', collapsed = false) {
		collapsed = collapsed && maxElementsInCell >= 3;
		const out =  $(`<tr class="${collapsed ? 'collapsed ' : ''}${classes}"><th scope="row" ${property != null ? `data-entity="${property}"` : ''}><span>${title}</span>${getCollapseButton()}</th>${cells || ''}</tr>`);
		if(property != null) {
			labelLoader.enqueueAndReplace(property, out.find('span a').toArray(), null);
		}
		return out;

		function getCollapseButton() {
			if(maxElementsInCell >= 3) {
				if(collapsed) {
					return '<a href="#collapse">[+]</a>';
				} else {
					return '<a href="#collapse">[–]</a>';
				}
			} else {
				return '';
			}
		}
	}
	
	function updateEntities(entities: { [key: string]: any }) {
		Object.keys(entities).forEach(function(val, i) {
			const thisEntity = entities[val];
			const label = LabelLoader.getLabel(thisEntity.labels, val);
			$(`th[scope="col"][data-entity="${val}"] a`).html(Ui.getIdLabel(val, label));
		});
	}

	function updateProperties(properties: string[]) {
		$.each(properties, function(i, property) {
			labelLoader.enqueueAndReplace(property, $(`th[scope="row"][data-entity="${property}"] a[href^='https://www.wikidata.org/']`).toArray(), null);
		});
	}

	function listProperties(entities: { [key: string]: any }): string[] {
		let properties: string[] = [];
		Object.keys(entities).forEach(function(wdId, i) {
			const thisEntity = entities[wdId];
			if(typeof thisEntity.claims != 'undefined') {
				properties = properties.concat(Object.keys(thisEntity.claims));
			} else {
				console.warn(`Id ${wdId} not found`);
			}					
		});

		return Utils.arrayUnique(properties).sort(function(firstEl, secondEl) {
			const firstNum = parseInt(firstEl.substr(1));
			const secondNum = parseInt(secondEl.substr(1));
			
			if(firstNum < secondNum) {
				return -1;
			} else if(firstNum == secondNum) {
				return 0;
			} else {
				return 1;
			}
		});
	}
}
function getEntities(ids: string[], props: string[], callback: (res: { [key: string]: any }) => void, callbackEr?: (e: Object) => void, requireFinishAll = true) {
	let idsCopy = ids.slice(0);
	const numberOfRequests = Math.ceil(ids.length / 50);
	let finishedRequests = 0;
	let allResults = {};

	while(idsCopy.length > 0) {
		const data: { [key: string]: string } = {
			'action': 'wbgetentities',
			'format': 'json',
			'origin': '*',
			'ids': idsCopy.slice(0,50).join('|')
		};
		if(props.length > 0) data.props = props.join('|');

		$.ajax({
			data: data,
			url: 'https://www.wikidata.org/w/api.php'
		}).done(function(e) {
			finishedRequests++;
			if(!requireFinishAll && typeof e.entities != 'undefined') {
				callback(e.entities);
			} else {
				if(typeof e.entities != 'undefined') {
					allResults = mergeResults(e.entities, allResults);
				}
				if(numberOfRequests <= finishedRequests) {
					callback(allResults);
				}
			}
		}).fail(function(e) {
			if(typeof callbackEr != 'undefined') callbackEr(e);
			console.error(e);
		});
		idsCopy = idsCopy.slice(50);
	}

	function mergeResults<T>(resNew: { [key: string]: T }, resAll: { [key: string]: T }) {
		Object.keys(resNew).forEach(function(key) {
			resAll[key] = resNew[key];
		});
		return resAll;
	}
}

function parseUserInput() {
	const userInputs: string[] = [];
	$.each((<string>$('#commands').val()).split('\n'), function(a, b) {
		b = b.replace(/ /, '');
		if(/^Q[0-9]+$/.test(b)) {
			userInputs.push(b);
		} else {
			console.warn('Invalid user input');
		}
	});
	return userInputs;
}

function wdDisplayAlias(alias: any) {
	return `<span class="bubble">${alias.language}</span> ${alias.value}`;
}

function wdDisplaySitelink(sitelink: any) {
	if(sitelink.site == 'commonswiki') {
		return getLink('commons.wikimedia.org');
	} else if(sitelink.site.match(/^(.{2,})wiki$/) != null) {
		const matches = sitelink.site.match(/^(.{2,})wiki$/);
		return getLink(`${matches[1]}.wikipedia.org`);
	} else if(sitelink.site.match(/^(.{2,})(wiki.*)$/) != null) {
		const matches = sitelink.site.match(/^(.{2,})(wiki.*)$/);
		return getLink(`${matches[1]}.${matches[2]}.org`);
	} else {
		// @TODO: Fix
		console.warn(`Unknown site ${sitelink.site}`);
		return getLink('example.org');
	}

	function getLink(host: string) {
		return `<a href="https://${host}/wiki/${encodeURIComponent(sitelink.title)}" target="_blank">${sitelink.site} <small>(${Utils._s(sitelink.title)})</small></a>`;
	}
}

function wdDisplayValue(claim: any) {
	if(claim.mainsnak.snaktype == 'somevalue') {
		return $('<i class="snak-somevalue">Some value</i>');
	} else if(claim.mainsnak.snaktype == 'novalue') {
		return $('<i class="snak-novalue">No value</i>');
	} else if(claim.mainsnak.snaktype == 'value') {
		if(typeof claim.mainsnak.datatype == 'undefined') {
			console.warn(`Errornous mainsnak ${claim.mainsnak}`, claim);
			return $('<i class="snak-errornous">Errornous</i>');
		} else if(claim.mainsnak.datatype == 'commonsMedia') {
			return $(`<a href="https://commons.wikimedia.org/wiki/File:${Utils._s(claim.mainsnak.datavalue.value)}" target="_blank">${Utils._s(claim.mainsnak.datavalue.value)}</a>`);
		} else if(claim.mainsnak.datatype == 'external-id') {
			return $(`<span>${Utils._s(claim.mainsnak.datavalue.value)}</span>`);
		} else if(claim.mainsnak.datatype == 'globe-coordinate') {
			return $(`<span>${Utils._s(`${claim.mainsnak.datavalue.value.latitude},${claim.mainsnak.datavalue.value.longitude}`)}</span>`);
		} else if(claim.mainsnak.datatype == 'monolingualtext') {
			return $(`<span>${Utils._s(claim.mainsnak.datavalue.value.text)} <small>(${Utils._s(claim.mainsnak.datavalue.value.language)})</small></span>`);
		} else if(claim.mainsnak.datatype == 'quantity') {
			// @TODO: Get display name
			if(claim.mainsnak.datavalue.value.unit.startsWith('http://www.wikidata.org/entity/')) {
				const wdId = claim.mainsnak.datavalue.value.unit.replace('http://www.wikidata.org/entity/', '');
				const out = $(`<span>${Utils._s(claim.mainsnak.datavalue.value.amount)} <small>(<a href="https://www.wikidata.org/wiki/${Utils._s(wdId)}" target="_blank" title="${wdId}" data-wdid="${wdId}">${Utils._s(wdId)}</a>)</small></span>`);
				labelLoader.enqueueAndReplace(wdId, out.find('small a').toArray(), null);
				return out;
			} else {
				if(claim.mainsnak.datavalue.value.unit == '1') {
					return $(`<span>${Utils._s(claim.mainsnak.datavalue.value.amount)}</span>`);
				} else {
					return $(`<span>${Utils._s(claim.mainsnak.datavalue.value.amount)} <small>(${claim.mainsnak.datavalue.value.unit})</small></span>`);
				}
			}
		} else if(claim.mainsnak.datatype == 'string') {
			return $(`<span>${Utils._s(claim.mainsnak.datavalue.value)}</span>`);
		} else if(claim.mainsnak.datatype == 'time') {
			return $(`<span>${Utils._s(`${claim.mainsnak.datavalue.value.time}/${claim.mainsnak.datavalue.value.precision}`)}</span>`);
		} else if(claim.mainsnak.datatype == 'url') {
			return $(`<a href="${Utils._s(claim.mainsnak.datavalue.value)}" target="_blank">${Utils._s(claim.mainsnak.datavalue.value)}</a>`);
		} else if(claim.mainsnak.datatype == 'wikibase-property') {
			const wdId = claim.mainsnak.datavalue.value.id;
			const out = $(`<a href="https://www.wikidata.org/wiki/Property:${Utils._s(wdId)}" target="_blank" title="${Utils._s(wdId)}">${Ui.getIdLabel(wdId, null)}</a>`);
			labelLoader.enqueueAndReplace(wdId, out.find('.idlabel').toArray(), null);
			return out;
		} else if(claim.mainsnak.datatype == 'wikibase-item') {
			const wdId = claim.mainsnak.datavalue.value.id;
			const out = $(`<a href="https://www.wikidata.org/wiki/${Utils._s(wdId)}" target="_blank" title="${Utils._s(wdId)}">${Ui.getIdLabel(wdId, null)}</a>`);
			labelLoader.enqueueAndReplace(wdId, out.find('.idlabel').toArray(), null);
			return out;
		} else {
			console.warn(`Unknown datatype ${claim.mainsnak.datatype}`, claim);
			return $('<i>Present</i>');
		}
	} else {
		console.warn(`Unknown snaktype ${claim.snaktype}`, claim);
		return $('<i>Unknown snaktype</i>');
	}
}
