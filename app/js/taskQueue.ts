import ReactiveQueue from './reactiveQueue';

export class StatusReporter {
	private _failTask: () => void
	get failTask(): () => void {
		return this._failTask;
	}
	private _finishTask: () => void
	get finishTask(): () => void {
		return this._finishTask;
	}

	constructor(finishTask: () => void, failTask: () => void) {
		this._failTask = failTask;
		this._finishTask = finishTask;
	}
}

export default class TaskQueue implements ReactiveQueue {
	max: number;
	nextTaskPointer: number[];
	performing: number;
	queue: { [key: number]: ((statusReporter: StatusReporter) => void)[] };

	taskCounter: number;
	currentHighestPriority: number;
	prioritiesCache: string[];

	constructor(max: number) {
		this.max = max;
		this.nextTaskPointer = [];
		this.queue = [];
		this.performing = 0;

		this.taskCounter = 0;
		this.currentHighestPriority = 0;
		this.prioritiesCache = [];
	}

	enqueueTask(priority: number): Promise<StatusReporter> {
		const that = this;
		return new Promise(function(resolve, _reject) {
			if(typeof that.queue[priority] === 'undefined') {
				that.queue[priority] = [];
				that.prioritiesCache = Array.from(Object.keys(that.queue));
				that.prioritiesCache.sort((l: string, r: string) => Number(r) - Number(l));
				that.nextTaskPointer[priority] = 0;
			}
			that.queue[priority].push(resolve);
			if(priority > that.currentHighestPriority) {
				that.currentHighestPriority = priority;
			}
			that.taskCounter++;
			that.work();
		});
	}
	
	finishTask(): void {
		this.performing -= 1;
		this.work();
	}

	setNextPriority(): void {
		const currentPriorityIndex = this.prioritiesCache.indexOf(String(this.currentHighestPriority));
		this.currentHighestPriority = Number(this.prioritiesCache[currentPriorityIndex + 1]);
	}

	work(): void {
		if(this.max > this.performing) {
			if(this.taskCounter <= 0) {
				console.debug('Queue finished');
				this.nextTaskPointer = [];
				this.queue = [];
				this.taskCounter = 0;
				this.currentHighestPriority = 0;
				this.prioritiesCache = [];
			} else if(this.nextTaskPointer[this.currentHighestPriority] >= this.queue[this.currentHighestPriority].length) {
				// Priority is done
				this.setNextPriority();
			} else {
				const currentPriorityQueue = this.queue[this.currentHighestPriority];
				const inprogress = currentPriorityQueue[this.nextTaskPointer[this.currentHighestPriority]];

				inprogress(new StatusReporter(() => this.finishTask(), () => this.finishTask()));

				this.performing += 1;
				this.nextTaskPointer[this.currentHighestPriority]++;
				this.taskCounter--;
			}
		}
	}
}