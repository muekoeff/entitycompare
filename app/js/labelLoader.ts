import Settings from './settings';
import TaskQueue from './taskQueue';

class QueueItem {
	callbackError: (object?: Object) => void
	callbackSuccess: (object?: Object) => void

	constructor(callbackSuccess: (object?: Object) => void, callbackError: (object?: Object) => void) {
		this.callbackSuccess = callbackSuccess;
		this.callbackError = callbackError;
	}
}

export default class LabelLoader {
	autoRequest: boolean
	cache: { [key: string]: Object }
	queue: { [key: string]: (QueueItem)[] }
	queueRequesting: Array<Object>
	queueToRequest: Array<string>
	taskQueue: TaskQueue

	static QueueItem = QueueItem

	constructor(taskQueue: TaskQueue) {
		this.autoRequest = false;
		this.cache = {};
		this.queue = {};
		this.queueRequesting = [];
		this.queueToRequest = [];
		this.taskQueue = taskQueue;
	}

	static getLabel(wikidataLabels: { [key: string]: any }, fallback?: string) {
		if(typeof wikidataLabels[Settings.getLanguage()] == 'object') {
			return wikidataLabels[Settings.getLanguage()].value;
		} else if(typeof wikidataLabels['en'] == 'object') {
			return wikidataLabels['en'].value;
		} else if(Object.keys(wikidataLabels).length > 0) {
			if(fallback !== undefined) {
				return fallback;
			} else {
				return wikidataLabels[Object.keys(wikidataLabels)[0]].value;
			}
		} else {
			if(fallback !== undefined) {
				return fallback;
			} else {
				return null;
			}
		}
	}

	enqueue(wdId: string): Promise<Object> {
		const that = this;

		return new Promise(function(resolve, reject) {
			if(typeof that.cache[wdId] == 'object') {
				resolve(that.cache[wdId]);
			} else {
				if(typeof that.queue[wdId] == 'undefined') that.queue[wdId] = [];
				
				that.queue[wdId].push(new LabelLoader.QueueItem(resolve, reject));
	
				if(that.queueRequesting.includes(wdId) && that.autoRequest) {
					that._enqueueRequest(wdId);
				} else {
					if(!that.queueToRequest.includes(wdId)) that.queueToRequest.push(wdId);
				}
			}
		});
	}

	enqueueAndReplace(wdId: string, elem: HTMLElement[], fallback?: string, forceLabel = false) {
		if(typeof fallback != 'string') fallback = wdId;

		this.enqueue(wdId).then(function(wikidataLabels) {
			if(forceLabel || !$('#setting-ui-displaywdid').prop('checked')) {
				if(!Array.isArray(elem)) elem = [elem];

				const label = LabelLoader.getLabel(wikidataLabels, fallback);
				if(label != null || typeof fallback == 'string') {
					$.each(elem, function(i, val) {
						val.textContent = label || fallback;
					});
				}
			}
		});
	}

	startWork() {
		const that = this;

		this._enqueueRequest(this.queueToRequest);
		$.each(this.queueToRequest, function(_i, val) {
			if(!that.queueRequesting.includes(val)) {
				that.queueRequesting.push(val);
			}
		});
		
		this.queueToRequest = [];
	}

	_enqueueRequest(wdIds: (string|string[])): void {
		if(typeof wdIds == 'string') wdIds = [wdIds];

		const that = this;
		let ids = wdIds.slice(0);
		
		while(ids.length > 0) {
			const data = {
				idsSegment: ids.slice(0,50)
			};
			this.taskQueue.enqueueTask(0).then(function(statusReporter) {
				$.ajax({
					data: {
						'action': 'wbgetentities',
						'props': 'labels',
						'format': 'json',
						'languages': `${Settings.getLanguage()}|en`,
						'origin': '*',
						'ids': data.idsSegment.join('|')
					},
					url: 'https://www.wikidata.org/w/api.php'
				}).always(function(_e) {
					statusReporter.finishTask();
				}).done(function(e) {
					console.debug(e);
					$.each(data.idsSegment, function(i, wdId) {
						const queueRequestingPosition = that.queueRequesting.indexOf(wdId);
						that.queueRequesting.splice(queueRequestingPosition);

						$.each(that.queue[wdId], function(j, val) {
							if(typeof e.entities == 'object' && typeof e.entities[wdId] == 'object') {
								val.callbackSuccess(e.entities[wdId].labels);
							} else if(typeof val.callbackError == 'function') {
								val.callbackError();
							}
						});
						delete that.queue[wdId];
					});
				}).fail(function(e) {
					$.each(data.idsSegment, function(_i, wdId) {
						const queueRequestingPosition = that.queueRequesting.indexOf(wdId);
						that.queueRequesting.splice(queueRequestingPosition);

						$.each(that.queue[wdId], function(_i, val) {
							if(typeof val.callbackError == 'function') {
								val.callbackError();
							}
						});
						delete that.queue[wdId];
					});
				});
			});
			ids = ids.slice(50);
		}
	}
}
