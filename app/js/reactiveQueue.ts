export default interface ReactiveQueue {
	finishTask(): void;
}
