import Settings from './settings';

export default class Permalink {
	static generatePermalink(userInput: string[]): string {
		const parameters = [];

		// Options
		const label = <string>$('#permalink-label').val();
		const includedItems = $('#permalink-includeditems').prop('checked');
		const filterProperties = $('#permalink-filterproperties').prop('checked');
		const displayedProperties: string[] = [];
		$('#permalink-displayedproperties').find(':checked').each(function(_i, _val) {
			displayedProperties.push((<string>$(this).val()));
		});
		const autoload = $('#permalink-queryonloaded').prop('checked');
		const fullscreen = $('#permalink-fullscreen').prop('checked');
		const settings = $('#permalink-settings').prop('checked');

		if(label !== null) parameters.push(`l=${encodeURIComponent(label)}`);
		if(includedItems) parameters.push(`i=${encodeURIComponent(userInput.join(','))}`);
		if(filterProperties) parameters.push(`p=${encodeURIComponent(displayedProperties.join(','))}`);
		if(autoload) parameters.push('r');
		if(fullscreen) parameters.push('f');
		if(settings) parameters.push(`settings=${encodeURIComponent(JSON.stringify(Settings.export()))}`);

		return `${document.location.protocol}//${document.location.host}${document.location.pathname}?${parameters.join('&')}`;
	}

	static getFilteredProperties(): string[] {
		const urlParams = new URLSearchParams(window.location.search);
		if(urlParams.get('p') != null) {
			return urlParams.get('p').split(',');
		} else {
			return null;
		}
	}
    
	static load(): void {
		const urlParams = new URLSearchParams(window.location.search);
		if(urlParams.get('i') != null) {
			$('#commands').val(urlParams.get('i').split(',').join('\n'));
	
			// Auto-execute?
			if(urlParams.has('r')) {
				$('#button-parse').trigger('click');
	
				if(urlParams.has('f')) {
					$('#table-container').toggleClass('fullscreen');
				}
			}
		}
	}
    
	static uiShown(userInput: string[], properties: string[]): void {
		$('#permalink-displayedproperties').empty();
		$.each(properties, function(i, val) {
			$('#permalink-displayedproperties').append(`<option selected>${val}</option>`);
		});
		$('#modal-permalink input, #modal-permalink select').on('change keyup paste', function(_e) {
			Permalink.uiUpdate(userInput);
		});
		Permalink.uiUpdate(userInput);
	}
    
	static uiUpdate(userInput: string[]): void {
		if($('#permalink-includeditems').prop('checked')) {
			$('#permalink-queryonloaded').removeAttr('disabled');
		} else {
			$('#permalink-queryonloaded').attr('disabled', 'disabled');
		}
		if($('#permalink-filterproperties').prop('checked')) {
			$('#permalink-displayedproperties').removeAttr('disabled');
		} else {
			$('#permalink-displayedproperties').attr('disabled', 'disabled');
		}
		Permalink.uiUpdateUrl(userInput);
	}
    
	static uiUpdateUrl(userInput: string[]): void {
		const permalink = Permalink.generatePermalink(userInput);
		$('#button-permalink-open').attr('href', permalink);
		$('#permalink-output').val(permalink);
	}
}