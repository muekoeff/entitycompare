export default class Utils {
	static arrayUnique<T>(array: T[]): T[] {
		const a = array.concat();
		for(let i = 0; i < a.length; ++i) {
			for(let j = i+1; j < a.length; ++j) {
				if(a[i] === a[j]) a.splice(j--, 1);
			}
		}
	
		return a;
	}

	static copyTextToClipboard(text: string): boolean {
		const textArea = document.createElement('textarea');

		textArea.style.position = 'fixed';
		textArea.style.top = '0';
		textArea.style.left = '0';
		textArea.style.width = '2em';
		textArea.style.height = '2em';
		textArea.style.padding = '0';
		textArea.style.border = 'none';
		textArea.style.outline = 'none';
		textArea.style.boxShadow = 'none';
		textArea.style.background = 'transparent';
		textArea.value = text;
		document.body.appendChild(textArea);
		textArea.select();

		try {
			document.execCommand('copy');
			document.body.removeChild(textArea);
			return true;
		} catch(e) {
			console.error(e);
			document.body.removeChild(textArea);
			return false;
		}
	}

	static validateNumber(i: number, fallback: number): number {
		return (isNaN(i) ? fallback : i);
	}

	/**
	 * Sanitises a given string
	 *
	 * @param	{string}	text	Text to sanitise
	 * @return	{string} Sanitised string
	 */
	static _s(text: string): string {
		return $('<div/>').text(text).html();
	}
}
