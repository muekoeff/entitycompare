import Utils from './utils';

export default class Ui {
	static getIdLabel(wdId: string, label: string = null): string {
		return `<span class="idlabel" ${
			label == null ? '' : `data-label="${label}" `
		} data-wdid="${
			Utils._s(wdId)
		}">${
			($('#setting-ui-displaywdid').prop('checked') || label == null) ? Utils._s(wdId) : label
		}</span>`;
	}
}