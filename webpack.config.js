const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	devtool: 'source-map',
	entry: './app/js/main.ts',
	externals: {
		jquery: 'jQuery'
	},
	plugins: [
		new CleanWebpackPlugin({
			cleanAfterEveryBuildPatterns: ['public']
		}),
		new CopyPlugin({
			patterns: [
				{ from: './app/css/', to: 'data/css/' },
				{ from: './node_modules/bootstrap/dist/css/bootstrap.min.css', to: 'data/components/bootstrap/bootstrap.min.css' },
				{ from: './node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', to: 'data/components/bootstrap/bootstrap.bundle.min.js' },
				{ from: './node_modules/bootstrap/dist/js/bootstrap.bundle.min.js.map', to: 'data/components/bootstrap/bootstrap.bundle.min.js.map' },
				{ from: './node_modules/jquery/dist/jquery.min.js', to: 'data/components/jquery/jquery.min.js' },
				{ from: './node_modules/jquery/dist/jquery.min.map', to: 'data/components/jquery/jquery.min.map' },
				{ from: './node_modules/tooltipster/dist/', to: 'data/components/tooltipster/' },
				{ from: './toolinfo.json', to: 'toolinfo.json' }
			]
		}),
		new HtmlWebpackPlugin({
			template: './app/index.html',
			title : 'EntityCompare'
		}),
	],
	output: {
		path: __dirname + '/public',
		filename: '[name].js'
	},
	resolve: {
		extensions: ['.css', '.js', '.svg', '.ts', '.tsx']
	},
	module: {
		rules: [
			{ test: /\.css$/i, use: ['style-loader', 'css-loader'] },
			{ test: /\.svg$/i, use: [
				{
				  loader: 'file-loader',
				},
			  ]
			},
			{ test: /\.tsx?$/i, use: 'ts-loader' },
			{ test: /\.txt$/i, use: 'raw-loader' }
		]
	}
}
